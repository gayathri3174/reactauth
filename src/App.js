// App.js

import React from 'react';
import GoogleSignInButton from './GoogleSignInButton';

function App() {
  return (
    <div>
      <h1>Your React App</h1>
      <GoogleSignInButton />
    </div>
  );
}

export default App;
