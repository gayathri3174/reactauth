// firebase.js
import { initializeApp } from 'firebase/app';
import { getAuth, GoogleAuthProvider } from 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyCDoqPXnMASjsrJ7PKxcRLFbKV1Yx9vWCU",
  authDomain: "auth-react-c6f65.firebaseapp.com",
  projectId: "auth-react-c6f65",
  storageBucket: "auth-react-c6f65.appspot.com",
  messagingSenderId: "142572665842",
  appId: "1:142572665842:web:963f02d49ab7420806896b",
  measurementId: "G-SDE64CY677"
};

// Check if firebase.apps is empty to avoid re-initializing
const firebaseApp = initializeApp(firebaseConfig);

const auth = getAuth(firebaseApp);
const provider = new GoogleAuthProvider();

export { auth, provider, firebaseApp as default };
